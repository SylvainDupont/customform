<?php

/*
Plugin Name: Formulaire de contact - Demande de formation
Plugin URI: http://absolem.com
Description: Formulaire de contact - Demande de formation pour Accord paille
Version: 0.1
Author: DUPONT Sylvain (sylvain@absolem.com)
Author URI: http://absolem.com
License: GPL2
*/


// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

register_activation_hook( __FILE__, array( 'Customcontact', 'plugin_activation' ) );
register_deactivation_hook( __FILE__, array( 'Customcontact', 'plugin_deactivation' ) );


define( 'CC__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

require_once( CC__PLUGIN_DIR . 'class.customcontact.php' );

add_action( 'init', array( 'Customcontact', 'init' ) );
