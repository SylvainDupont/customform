<?php

class Customcontact {
    private static $initiated = false;

    public static function init() {
  		if ( ! self::$initiated ) {
  			self::init_hooks();
  		}
  	}

  	/**
  	 * Initializes WordPress hooks
  	 */
  	private static function init_hooks() {
  		self::$initiated = true;
  		add_action('admin_menu', array('Customcontact', 'add_admin_menu'));
      add_shortcode('demande_form_saisie', array('Customcontact', 'form_html'));
  	}

    public static function plugin_activation()
    {
        try{
            global $wpdb;
	          $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}cc_demandes;");
            $wpdb->query("CREATE TABLE IF NOT EXISTS {$wpdb->prefix}cc_demandes (id INT AUTO_INCREMENT PRIMARY KEY,
                nom VARCHAR(255) NOT NULL,
                prenom VARCHAR(255) NOT NULL,
                mail VARCHAR(255) NOT NULL,
                tel VARCHAR(255) NOT NULL,
                aide_opca BOOLEAN NOT NULL,
                type_demandeur VARCHAR(255) NOT NULL,
                nbr_participant INT NOT NULL,
                is_societaire BOOLEAN NOT NULL
            );");
	          $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}cc_listeformation;");
            $wpdb->query("CREATE TABLE IF NOT EXISTS {$wpdb->prefix}cc_listeformation (id INT AUTO_INCREMENT PRIMARY KEY,
                nom VARCHAR(255) NOT NULL
            );");
            $wpdb->query("INSERT INTO {$wpdb->prefix}cc_listeformation (`nom`) VALUES ('Module 1 : La conception passive et écologique')");
            $wpdb->query("INSERT INTO {$wpdb->prefix}cc_listeformation (`nom`) VALUES ('Module 2 : FDES – ACV – E+/C-')");
            $wpdb->query("INSERT INTO {$wpdb->prefix}cc_listeformation (`nom`) VALUES ('Module 3 : La composition des parois')");
            $wpdb->query("INSERT INTO {$wpdb->prefix}cc_listeformation (`nom`) VALUES ('Module 4 : ACV et Maquette numérique avec COCON-BIM')");
            $wpdb->query("INSERT INTO {$wpdb->prefix}cc_listeformation (`nom`) VALUES ('Module 5 : La réhabilitation à haut niveau de performances')");
            $wpdb->query("INSERT INTO {$wpdb->prefix}cc_listeformation (`nom`) VALUES ('Module 6 : La démarche low-tech')");
            $wpdb->query("INSERT INTO {$wpdb->prefix}cc_listeformation (`nom`) VALUES ('Module 7 : Choisir ses menuiseries')");
            $wpdb->query("INSERT INTO {$wpdb->prefix}cc_listeformation (`nom`) VALUES ('Module 8 : La ventilation et le renouvellement de l’air')");
            $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}cc_choix_modules;");
            $wpdb->query("CREATE TABLE IF NOT EXISTS {$wpdb->prefix}cc_choix_modules (id INT AUTO_INCREMENT PRIMARY KEY,
                id_form INT NOT NULL,
                id_module INT NOT NULL
            );");

        }
        catch(\Exception $e){
            var_dump($e->getMessage());
            wp_die();
        }
    }
    public static function plugin_deactivation()
    {
        global $wpdb;
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}devis_saisie;");
    }

    public static function add_admin_menu()
    {
        add_menu_page('Demandes', 'Demandes de formations', 'manage_options', 'customcontact', array('Customcontact', 'add_menu'));
    }
    public static function add_menu()
    {
        global $wpdb;
      /*  if(isset($_GET['delete'])):
                $wpdb->query("DELETE FROM {$wpdb->prefix}devis_saisie WHERE id=".$_GET['delete']);
                $file = dirname(__FILE__).'/../../../temp/devis_'.$_GET['delete'].'.pdf';
                @unlink($file);
        endif;*/
        echo '<h1>Liste des demandes </h1>';
        //wp_enqueue_script( 'devis-plugin-js', plugins_url( '/js/devis_plugin.js', __FILE__ ));

      //  $row = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}devis_saisie ORDER BY date_demande DESC");
        echo "<div class='card' style='width:95%;max-width:95%;float:left'>";
            echo "<table style='width:95%;border-collapse:collapse'>";
                echo "<thead>";
                    echo "<tr>";
                        echo "<th>N°</th><th style='text-align:right'>Type</th><th style='text-align:right'>Nombre</th><th>Période</th><th>du</th><th>au</th><th>Options</th><th style='text-align:right'>Total</th><th style='text-align:right'>Action</th>";
                    echo "</tr>";
                echo "</thead>";
                echo "<tbody>";
                    echo "<tr><td>teest</td></tr>";
                echo "</tbody>";
            echo "</table>";
        echo "</div>";
    }

    public static function form_html($atts, $content)    {
        global $wpdb;
          $row = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}devis_settings ORDER BY DateAjout DESC LIMIT 0,1");
          echo '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">';
        //  wp_enqueue_style( 'devis-parametre-css',
        wp_enqueue_style( 'form_front', plugins_url( '/css/form_front.css', __FILE__ ));
        //  wp_enqueue_style( 'devis-parametre-css-datetimepicker', plugins_url( '/css/jquery.datetimepicker.min.css', __FILE__ ));
        //  wp_enqueue_script( 'devis-parametre-js-datetimepicker', plugins_url( '/js/jquery.datetimepicker.full.min.js', __FILE__ ),array(),false,true);
         wp_enqueue_script( 'devis-parametre-js', plugins_url( '/js/form_front.js', __FILE__ ),array(),false,true);

            if(!isset($_POST['form_devis_submit'])):
              $html = Customcontact::render_form($row,$_GET);
              echo "<div class='formDevis'>".$html."</div>";
            else:
              // $Ddebut = explode(" ",$_POST['date_debut']);
              // $DdebutDate = explode("/",$Ddebut[0]);
              // $DateDebut = $DdebutDate[1].'/'.$DdebutDate[0].'/'.$DdebutDate[2].' '.$Ddebut[1];
              // $Dfin = explode(" ",$_POST['date_fin']);
              // $DfinDate = explode("/",$Dfin[0]);
              // $DateFin = $DfinDate[1].'/'.$DfinDate[0].'/'.$DfinDate[2].' '.$Dfin[1];
              // $DateFinCalculMidi = $DfinDate[1].'/'.$DfinDate[0].'/'.$DfinDate[2].' 12:00:00';
              // $dateDebut = new \DateTime($DateDebut);
              // $dateFin = new \DateTime($DateFin);
              // $dateFinCalculMidi = new \DateTime($DateFinCalculMidi);
              // $interval = date_diff($dateDebut, $dateFin);
              // $isAM = $dateFinCalculMidi < $dateFin;
              // $hf  = explode(":",$Dfin[1]);
              // $hd  = explode(":",$Ddebut[1]);
              // $majorj = 0;
              // if($hf[0]>12 && $hd[0]>12 && $hf[0]<$hd[0]):
              //   $majorj++;
              // endif;
              //     if($_POST['type_resident']=="chien"):
              //         $tarifs = $this->calcul_tarif_chiens($dateDebut,$interval->days+$majorj,$isAM,$_POST);
              //       else:
              //         $tarifs = $this->calcul_tarif_chats($dateDebut,$interval->days+$majorj,$isAM,$_POST);
              //       endif;
              //     echo "<div class='wpb_raw_code wpb_content_element wpb_raw_html'>";
              //         echo "<div class='wpb_column vc_column_container vc_col-sm-12'>";
              //             echo "<div class='resultDevis'>";
              //                 echo "<div class='wpb_column vc_column_container vc_col-sm-12'>";
              //                 echo "<h1>Votre devis* : </h1>";
              //                         echo "<p>Vous avez demandé une réservation pour ".$_POST['nbr_resident'].' '.ucfirst($_POST['type_resident']).(($_POST['nbr_resident']>1) ? 's':'')."</p>";
              //                         echo "<p><strong>Date d'arrivée demandée : </strong>".$dateDebut->format('d/m/Y')." à ".$dateDebut->format("H:i")."</p>";
              //                         echo "<p><strong>Date de départ demandée : </strong>".$dateFin->format('d/m/Y')." à ".$dateFin->format("H:i")."</p>";
              //
              //
              //                         if(isset($_POST['webcam']) || isset($_POST['television']) || isset($_POST['medicament']) || isset($_POST['brossage'])):
              //                                 echo "<p><strong>Options demandées :</strong></p>";
              //                                 echo "<ul>";
              //                                         if(isset($_POST['webcam'])):
              //                                                 echo "<li>Webcam</li>";
              //                                         endif;
              //                                         if(isset($_POST['television'])):
              //                                                 echo "<li>Télévision</li>";
              //                                         endif;
              //                                         if(isset($_POST['medicament'])):
              //                                                 echo "<li>Traitement médicament</li>";
              //                                         endif;
              //                                         if(isset($_POST['brossage'])):
              //                                                 echo "<li>Brossage, nettoyage des oreilles, ...</li>";
              //                                         endif;
              //                                 echo "</ul>";
              //                         endif;
              //
              //                         echo "<p><strong>Nombre de jour facturé : </strong>".$tarifs['nbNuits']."</p>";
              //                         echo "<hr />";
              //                 echo "</div>";
              //         echo "</div>";
              //
              //         echo "Soit un prix total de <strong>". $tarifs['prixFinal']." € </strong><br /><br />";
  		        //         echo "Un accompte de <strong>". $tarifs['accompte']." € </strong> vous sera demandé";			echo "<hr />";			echo "<i>« Ce devis n’a pas valeur de réservation. <br >Une réservation est effective après la visite de l’établissement. »</i>";
              //
              //
              //
              endif;

      }

      public static function render_form($row,$get){
          global $wpdb;
          $row = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}cc_listeformation ORDER BY nom ASC");
          $html="<div class='container'>";
            $html.="<form method='POST' action='#' name='formdemande'>";
                  $html.="<div class='row'>";
                          $html.="<div class='col-12'>";
                            $html.='<div class="form-group">
                              <label for="nom">Nom :</label>
                              <input type="text" class="form-control" id="nom">
                            </div>';
                          $html.="</div>";
                          $html.="<div class='col-12'>";
                            $html.='<div class="form-group">
                              <label for="prenom">Prénom :</label>
                              <input type="text" class="form-control" id="prenom">
                            </div>';
                          $html.="</div>";
                          $html.="<div class='col-12'>";
                            $html.='<div class="form-group">
                              <label for="email">Email :</label>
                              <input type="email" class="form-control" id="email">
                            </div>';
                          $html.="</div>";
                          $html.="<div class='col-12'>";
                            $html.='<div class="form-group">
                              <label for="tel">Téléphone :</label>
                              <input type="text" class="form-control" id="tel">
                            </div>';
                          $html.="</div>";
                          $html.="<div class='col-12'>";
                            $html.='<div class="form-group">
                              <label for="aideopca">Je bénéficie d\'une aide de l\'OPCA :</label>
                              <div class="input-group-text" style="background:#fff;border:none">
                                <input type="radio" name="aideopca[]" value="0">Non
                              </div>
                              <div class="input-group-text" style="background:#fff;border:none">
                                <input type="radio" name="aideopca[]" value="1">Oui
                              </div>
                            </div>';
                          $html.="</div>";
                          $html.="<div class='col-12'>";
                            $html.='<div class="form-group">
                              <label for="typedemandeur">Je suis :</label>
                              <div class="input-group-text" style="background:#fff;border:none">
                                <input type="radio" name="typedemandeur[]" value="pro">Entreprise
                              </div>
                              <div class="input-group-text" style="background:#fff;border:none">
                                <input type="radio" name="typedemandeur[]" value="part">Particulier
                              </div>
                            </div>';
                          $html.="</div>";
                          $html.="<div class='col-12'>";
                            $html.='<div class="form-group">
                              <label for="nbr_participant">Nombre de participant(s):</label>
                              <div class="input-group-text" style="background:#fff;border:none">
                                <input type="radio" name="nbr_participant" value="unique">1 seul participant
                              </div>
                              <div class="input-group-text" style="background:#fff;border:none">
                                <input type="radio" name="nbr_participant" value="plusieurs">Plusieurs participants
                              </div>
                            </div>';
                          $html.="</div>";
                          $html.="<div class='col-12 tohide reveal_liste_formations'>";
                            $html.='<div class="form-group hidden" id="liste_formations">
                              <label for="liste_modules">Je choisis les modules de formation :</label>';
                                foreach($row as $r):
                                  $html.='<div class="input-group-text" style="background:#fff">';
                                    $html.='<input type="checkbox" name="liste_modules[]" id="liste_modules[]" value="'.$r->id.'">'.$r->nom;
                                $html.='</div>';
                                endforeach;
                            $html.='</div>';
                          $html.="</div>";
                          $html.="<div class='col-12 tohide reveal_societaire_ou_pas'>";
                            $html.='<div class="form-group">
                              <label for="societaire_ou_pas">Je suis :</label>
                              <select class="form-control" id="societaire_ou_pas">
                                <option value="0">Non sociétaire</option>
                                <option value="1">Sociétaire</option>
                              </select>
                            </div>';
                          $html.="</div>";
                          $html.="<div class='col-12'>";
                            $html.='<center><input class="btn btn-success"  type="submit" name="validDemande" value="Je valide"></center>';
                          $html.='</div>';
                  $html.="</div>";
          $html.="</form>";
          $html.="</div>";

          return $html;
      }

}
