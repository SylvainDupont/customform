jQuery('.dashicons-no').on("click",function(){
   if(!confirm("Etes-vous sur de vouloir supprimer ce devis ?")){
	   return false;
   }
});
jQuery('#exportEmail').on("click",function(){
  jQuery.ajax({
       url: ajaxurl,
       method: "POST",
       dataType:"json",
       data: { 'action': 'export_mail'}
  }).done(function(data) {
       if(data.success){
         jQuery('#retourmail').html("<a style='font-size:22px;color:red' href='"+data.data+"' target='_blank'>Télécharger le fichier d'exportation de mails</a>");
       }
       else{
           alert("Une erreur est survenue");
       }
  });
});
