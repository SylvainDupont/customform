jQuery.datetimepicker.setLocale('fr');

jQuery(document).ready(function(){
    var dDate = new Date();
    jQuery('#date_debut').datetimepicker({
        i18n:{
            fr:{
             months:[
              'Janvier','Février','Mars','Avril',
              'Mai','Juin','Juillet','Août',
              'Septembre','Octobre','Novembre','Décembre'
             ],
             dayOfWeek:[
              "Dim", "Lun", "Mar", "Mer", 
              "Jeu", "Ven", "Sam",
             ]
            }
           },
        format:'d/m/Y H:i',
        lang:'fr',
        mask:true,
        yearStart:dDate.getFullYear(),
        value:(dDate.getDate())+'/'+(dDate.getMonth()+1)+'/'+dDate.getFullYear()+' '+dDate.getHours(),
        minDate:dDate.getDate()+'/'+(dDate.getMonth()+1)+'/'+dDate.getFullYear()+' '+dDate.getHours(),
        onChangeDateTime:function(ct,$i){
            jQuery('#date_fin').datetimepicker('setOptions', {minDate:jQuery('#date_debut').datetimepicker('getValue')});
            if(jQuery('#date_fin').datetimepicker('getValue')<jQuery('#date_debut').datetimepicker('getValue')){
                jQuery('#date_fin').datetimepicker('setOptions', {value:jQuery('#date_debut').datetimepicker('getValue')});
            }
        }
    });
    jQuery('#date_fin').datetimepicker({
        i18n:{
            fr:{
             months:[
              'Janvier','Février','Mars','Avril',
              'Mai','Juin','Juillet','Août',
              'Septembre','Octobre','Novembre','Décembre'
             ],
             dayOfWeek:[
              "Dim", "Lun", "Mar", "Mer", 
              "Jeu", "Ven", "Sam",
             ]
            }
           },
        format:'d/m/Y H:i',
        lang:'fr',
        mask:true,
        yearStart:dDate.getFullYear(),
        value:(dDate.getDate()+1)+'/'+(dDate.getMonth()+1)+'/'+dDate.getFullYear()+' '+dDate.getHours(),
        minDate:dDate.getDate()+'/'+(dDate.getMonth()+1)+'/'+dDate.getFullYear()+' '+dDate.getHours()
    });
    jQuery('#date_debut').click(function(){
        jQuery('#date_debut').datetimepicker('show'); //support hide,show and destroy command
    });
    jQuery('#date_fin').click(function(){
        jQuery('#date_fin').datetimepicker('show'); //support hide,show and destroy command
    });
});

jQuery('input[type=radio][name=type_resident]').on("click",function(){
   if(jQuery(this).val()=="chien"){
       jQuery('#nbr_resident3').hide();
       jQuery('#nbr_resident3').parent().hide();
       jQuery('#nbr_resident1').trigger("click");
       
   }
   else{
       
       jQuery('#nbr_resident3').show();
       jQuery('#nbr_resident3').parent().show();
   }
});