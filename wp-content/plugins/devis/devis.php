<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of devis
 *
 * @author Sylvain
 */
//include_once plugin_dir_path( __FILE__ ).'/Devis_Ajax.php';

class Devis {
    
    public function __construct()
    {        
        add_action('admin_menu', array($this, 'add_admin_menu'));
    }
    
    
    public static function install()
    {
        global $wpdb;
	$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}devis_saisie;");
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}devis_settings;");
        $wpdb->query("CREATE TABLE IF NOT EXISTS {$wpdb->prefix}devis_saisie (id INT AUTO_INCREMENT PRIMARY KEY, 
            type_resident VARCHAR(25) NOT NULL,
            nbr_resident INT NOT NULL,
            type_periode INT NOT NULL,
            date_debut DATETIME NOT NULL,
            date_fin DATETIME NOT NULL,
            opt_webcam BOOLEAN NOT NULL,
            opt_television BOOLEAN NOT NULL,
            opt_traitement BOOLEAN NOT NULL,
            opt_brossage BOOLEAN NOT NULL,
            date_demande DATETIME NOT NULL,
            TotalPRIX FLOAT NOT NULL,
            IDSettings INT NOT NULL
        );");
        $wpdb->query("CREATE TABLE IF NOT EXISTS {$wpdb->prefix}devis_settings (id INT AUTO_INCREMENT PRIMARY KEY, 
            email_contact VARCHAR(255) NOT NULL,
            taux_reduc_nuit_6 INT NOT NULL,
            taux_reduc_nuit_10 INT NOT NULL,
            taux_accompte INT NOT NULL,
            prix_webcam FLOAT NOT NULL,
            prix_television FLOAT NOT NULL,
            prix_medicament FLOAT NOT NULL,
            prix_brossage FLOAT NOT NULL,
            hvs_chat FLOAT NOT NULL,
            hvs_chien FLOAT NOT NULL,
            vsjf_chien FLOAT NOT NULL,
            vsjf_chat FLOAT NOT NULL,
            majoration_am_chien FLOAT NOT NULL,
            majoration_am_chat FLOAT NOT NULL,
            majoration_chien_2 FLOAT NOT NULL,
            majoration_chat_2 FLOAT NOT NULL,
            majoration_chat_3 FLOAT NOT NULL,
            txt_periode TEXT NULL,
            txt_question TEXT NULL,
            DateAjout DATETIME
        );");
        $wpdb->query("CREATE TABLE IF NOT EXISTS {$wpdb->prefix}devis_settings_temp (id INT AUTO_INCREMENT PRIMARY KEY,
            hvs_chat FLOAT NOT NULL,
            hvs_chien FLOAT NOT NULL,
            vsjf_chien FLOAT NOT NULL,
            vsjf_chat FLOAT NOT NULL,
            majoration_am_chien FLOAT NOT NULL,
            majoration_am_chat FLOAT NOT NULL,
            majoration_chien_2 FLOAT NOT NULL,
            majoration_chat_2 FLOAT NOT NULL,
            majoration_chat_3 FLOAT NOT NULL,
            DateAjout DATETIME
        );");
        $wpdb->query("CREATE TABLE IF NOT EXISTS {$wpdb->prefix}devis_jours_feries (id INT AUTO_INCREMENT PRIMARY KEY, 
            year INT NOT NULL,
            nom VARCHAR(255) NOT NULL,
            etat boolean NOT NULL,
            timestamp int NOT NULL
        );");
        $wpdb->query("CREATE TABLE IF NOT EXISTS {$wpdb->prefix}devis_vacances (id INT AUTO_INCREMENT PRIMARY KEY, 
            periode VARCHAR(255) NOT NULL,
            zone VARCHAR(255) NOT NULL,
            DateDebut DATETIME,
            DateFin DATETIME
        );");
        $wpdb->query("INSERT INTO {$wpdb->prefix}devis_settings 
            (`email_contact`,`taux_reduc_nuit_6`,`taux_reduc_nuit_10`,`taux_accompte`,`txt_periode`,`prix_webcam`,`prix_television`,`prix_medicament`,`prix_brossage`,`DateAjout`,`txt_question`,
            `hvs_chien`,`vsjf_chien`,`hvs_chat`,`vsjf_chat`,`majoration_am_chien`,`majoration_am_chat`,`majoration_chien_2`,`majoration_chat_2`,`majoration_chat_3`) 
            VALUES ('contact@demo.fr',10,10,50,
            'Le nettoyage des chambre se fait en général le midi sauf exception. Le temps supplémentaire sera alors compté comme une demie journée de réservation en plus.',5,5,5,5,NOW(),
            '<p>Le propriétaire fournit la nourriture de son animal.</p><p>Assurez vous de réaliser deux devis différents si vous souhaitez héberger un chien et un chat.</p><p>Pour toute question, contactez - nous au 09 83 32 10 55</p>',
            '25','28','20','20','11','0','50','50','50');");
        try{
            for($i=date('Y');$i<date('Y')+5;$i++):
                $timestamp = Devis::getJoursFeries($i,true);
                foreach($timestamp as $k=>$t):
                    $wpdb->query("INSERT INTO {$wpdb->prefix}devis_jours_feries (`year`,`timestamp`,`nom`,`etat`) VALUES (".$i.",".(array_keys($timestamp[$k])[0]).",'".$t[array_keys($timestamp[$k])[0]]."',1)");
                endforeach;
            endfor;
        }
        catch(\Exception $e){
            var_dump($e->getMessage());
        }
    }
    public static function uninstall()
    {
        global $wpdb;
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}devis_saisie;");
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}devis_settings;");
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}devis_vacances;");
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}devis_jours_feries;");
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}devis_settings_temp;");
    }
    
    
    public function devis_parametre()
    {
        global $wpdb;
        wp_enqueue_script( 'devis-parametre-js-ui', plugins_url( '/js/jquery-ui.min.js', __FILE__ ));
        wp_enqueue_script( 'devis-parametre-js', plugins_url( '/js/devis_parametre.js', __FILE__ ));
        wp_enqueue_style( 'devis-parametre-css-ui', plugins_url( '/css/jquery-ui.min.css', __FILE__ ));
        wp_enqueue_style( 'devis-parametre-css', plugins_url( '/css/devis_parametre.css', __FILE__ ));
        
        
        if(isset($_POST['form_devis_parametre_submit'])):
            $_POST['taux_reduc_nuit_10'] = ($_POST['taux_reduc_nuit_10']>100) ? 100 : $_POST['taux_reduc_nuit_10'];
            $_POST['taux_reduc_nuit_6'] = ($_POST['taux_reduc_nuit_6']>100) ? 100 : $_POST['taux_reduc_nuit_6'];
            $_POST['taux_accompte'] = ($_POST['taux_accompte']>100) ? 100 : $_POST['taux_accompte'];
            
            $reqUpdate = "
                INSERT INTO {$wpdb->prefix}devis_settings (
                    `email_contact`,`taux_reduc_nuit_6`,`taux_reduc_nuit_10`,`taux_accompte`,`txt_periode`,
                    `prix_webcam`,`prix_television`,`prix_medicament`,`prix_brossage`,
                    `DateAjout`,`txt_question`,`hvs_chat`,`hvs_chien`,
                    `vsjf_chien`,`vsjf_chat`,`majoration_am_chien`,`majoration_am_chat`,
                    `majoration_chien_2`,`majoration_chat_2`,`majoration_chat_3`
                ) 
                    VALUES
                    (
                        '".$_POST['email_contact']."','".$_POST['taux_reduc_nuit_6']."', 
                        '".$_POST['taux_reduc_nuit_10']."',
                        '".$_POST['taux_accompte']."',
                        '".$_POST['txt_periode']."',
                        '".$_POST['prix_webcam']."',
                        '".$_POST['prix_television']."',
                        '".$_POST['prix_medicament']."',
                        '".$_POST['prix_brossage']."',
                        NOW(),
                        '".$_POST['txt_question']."',
                        '".$_POST['hvs_chat']."',
                        '".$_POST['hvs_chien']."',
                        '".$_POST['vsjf_chien']."',
                        '".$_POST['vsjf_chat']."',
                        '".$_POST['majoration_am_chien']."',
                        '".$_POST['majoration_am_chat']."',
                        '".$_POST['majoration_chien_2']."',
                        '".$_POST['majoration_chat_2']."',
                        '".$_POST['majoration_chat_3']."'
                    )
                "; 
            $wpdb->query($reqUpdate);
        endif;
        $row = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}devis_settings ORDER BY DateAjout DESC LIMIT 0,1");
        
        
        echo '<h1>Devis - Paramètres</h1>';
        echo "<form method='POST' action='#' name='form_devis_parametre'>";
        
            
                
                echo "<div style='width: 45%;display: block;position: relative;float: left;z-index:10;padding:20px;'>";
                    echo "<h2>Calcul des tarifs : </h2>";
                    echo "<h3>Chiens : </h3>";
                    
                    echo "<label for='hvs_chien'><strong></strong>Tarif de base pour une nuit hors vacances scolaires : ";
                    echo "<input type='number' name='hvs_chien' id='hvs_chien' required='required' value='".$row->hvs_chien."'> €";
                    echo "<br />";
                    
                    echo "<label for='vsjf_chien'><strong></strong>Tarif de base pour une nuit pendant les vacances scolaires : ";
                    echo "<input type='number' name='vsjf_chien' id='vsjf_chien' required='required' value='".$row->vsjf_chien."'> €";
                    echo "<br />";
                    
                    echo "<label for='majoration_chien_2'>Taux de réduction sur deuxième chien : ";
                    echo "<input type='number' name='majoration_chien_2' id='majoration_chien_2' required='required' value='".$row->majoration_chien_2."'> %";
                    echo "<hr />";
                    echo "<h3>Chats : </h3>";
                    
                    echo "<label for='hvs_chat'><strong></strong>Tarif de base pour une nuit  : ";
                    echo "<input type='number' name='hvs_chat' id='hvs_chat' required='required' value='".$row->hvs_chat."'> €";
                    echo "<br />";
                    
//                    echo "<label for='vsjf_chat'><strong></strong>Tarif de base pour une nuit pendant les vacances scolaires : ";
//                    echo "<input type='number' name='vsjf_chat' id='vsjf_chat' required='required' value='".$row->vsjf_chat."'> %";
//                    echo "<br />";
                    
                    echo "<label for='majoration_chat_2'><strong></strong>Taux de réduction sur deuxième chat : ";
                    echo "<input type='number' name='majoration_chat_2' id='majoration_chat_2' required='required' value='".$row->majoration_chat_2."'> %";
                    echo "<br />";
                    
                    echo "<label for='majoration_chat_3'><strong></strong>Taux de réduction sur troisième chat : ";
                    echo "<input type='number' name='majoration_chat_3' id='majoration_chat_3' required='required' value='".$row->majoration_chat_3."'> %";
                    echo "<hr />";
                    
                    echo "<h2>Majoration et réduction : </h2>";
                    
                    echo "<label for='majoration_am_chien'>Majoration pour un chien si retour après midi : ";
                    echo "<input type='number' name='majoration_am_chien' id='majoration_am_chien' required='required' value='".$row->majoration_am_chien."'> €";
                    echo "<br />";
                    
                    echo "<label for='majoration_am_chat'>Majoration pour un chat si retour après midi : ";
                    echo "<input type='number' name='majoration_am_chat' id='majoration_am_chat' required='required' value='".$row->majoration_am_chat."'> €";
                    echo "<br />";
                    
                    echo "<label for='taux_reduc_nuit_6'>Taux du tarif dégressif pour la 6 ème nuit pour les chiens : ";
                    echo "<input type='number' name='taux_reduc_nuit_6' id='taux_reduc_nuit_6' required='required' value='".$row->taux_reduc_nuit_6."'> %";
                    echo "<br />";

                    echo "<label for='taux_reduc_nuit_10'>Taux du tarif dégressif pour la 10 ème nuit pour les chats : ";
                    echo "<input type='number' name='taux_reduc_nuit_10' id='taux_reduc_nuit_10' required='required' value='".$row->taux_reduc_nuit_10."'> %";
                    echo "<br />";

                    echo "<label for='taux_accompte'>Taux de l'accompte : ";
                    echo "<input type='number' name='taux_accompte' id='taux_accompte' required='required' value='".$row->taux_accompte."'> %";
                    echo "<br />";
                    
                    echo "<h2>Options : </h2>";
                    
                    echo "<label for='prix_webcam'>Tarif pour l'option <strong>Webcam / jour</strong> : ";
                    echo "<input type='number' name='prix_webcam' id='prix_webcam' required='required' value='".$row->prix_webcam."'> €";
                    echo "<br />";

                    echo "<label for='prix_television'>Tarif pour l'option <strong>Télévision / jour</strong> : ";
                    echo "<input type='number' name='prix_television' id='prix_television' required='required' value='".$row->prix_television."'> €";
                    echo "<br />";
                    
                    echo "<label for='prix_medicament'>Tarif pour l'option <strong>Traitement médicament / jour / animal</strong> : ";
                    echo "<input type='number' name='prix_medicament' id='prix_medicament' required='required' value='".$row->prix_medicament."'> €";
                    echo "<br />";
                    
                    echo "<label for='prix_brossage'>Tarif pour l'option <strong>Brossage, nettoyage des oreilles, ... / jour / animal</strong> : ";
                    echo "<input type='number' name='prix_brossage' id='prix_brossage' required='required' value='".$row->prix_brossage."'> €";
                    
                echo "</div>";
                
                echo "<div style='width: 50%;display: block;position: relative;float: left;z-index:10;padding:20px'>";
                    echo "<h2>Simulation : </h2>";
                    
                    echo "
                        <div id='tabs'>
                            <ul> 
                                <li><a href='#tabs-1'>CHIENS - VACANCES SCOLAIRES</a></li>
                                <li><a href='#tabs-2'>CHIENS - HORS VACANCES SCOLAIRES</a></li>
                                <li><a href='#tabs-3'>CHATS</a></li>
                            </ul>  ";  
                        echo "<div id='tabs-1'>";

                                echo "<table id='simulationchienvs' class='tbl_devis_calcul_tarif table-responsive'>";
                                    echo "<thead>";
                                        echo "<tr>";
                                            echo "<th colspan='7'><strong><center>VACANCES SCOLAIRES</center></strong></th>";
                                        echo "</tr>";
                                        echo "<tr>";
                                            echo "<th colspan='1'></th>";
                                            echo "<th colspan='3'><strong><center>UN CHIEN</center></strong></th>";
                                            echo "<th colspan='3'><strong><center>DEUX CHIENS</center></strong></th>";
                                        echo "</tr>";
                                        echo "<tr>";
                                            echo "<th><strong><center>Nbr nuits</center></strong></th>";
                                            echo "<th><strong><center>Ret. avant midi</center></strong></th>";
                                            echo "<th><strong><center>Ret. après midi</center></strong></th>";
                                            echo "<th><strong><center>Réduction</center></strong></th>";
                                            echo "<th><strong><center>Ret. avant midi</center></strong></th>";
                                            echo "<th><strong><center>Ret. après midi</center></strong></th>";
                                            echo "<th><strong><center>Réduction</center></strong></th>";
                                        echo "</tr>";
                                    echo "</thead>";
                                    echo "<tbody>";

                                    echo "</tbody>";
                                echo "</table>";
                            echo "</div>";
                        echo "<div id='tabs-2'>";

                                echo "<table id='simulationchienhvs' class='tbl_devis_calcul_tarif table-responsive'>";
                                    echo "<thead>";
                                        echo "<tr>";
                                            echo "<th colspan='7'><strong><center>HORS VACANCES SCOLAIRES</center></strong></th>";
                                        echo "</tr>";
                                        echo "<tr>";
                                            echo "<th colspan='1'></th>";
                                            echo "<th colspan='3'><strong><center>UN CHIEN</center></strong></th>";
                                            echo "<th colspan='3'><strong><center>DEUX CHIENS</center></strong></th>";
                                        echo "</tr>";
                                        echo "<tr>";
                                            echo "<th><strong><center>Nbr nuits</center></strong></th>";
                                            echo "<th><strong><center>Ret. avant midi</center></strong></th>";
                                            echo "<th><strong><center>Ret. après midi</center></strong></th>";
                                            echo "<th><strong><center>Réduction</center></strong></th>";
                                            echo "<th><strong><center>Ret. avant midi</center></strong></th>";
                                            echo "<th><strong><center>Ret. après midi</center></strong></th>";
                                            echo "<th><strong><center>Réduction</center></strong></th>";
                                        echo "</tr>";
                                    echo "</thead>";
                                    echo "<tbody>";

                                    echo "</tbody>";
                                echo "</table>";
                            echo "</div>";
                        echo "<div id='tabs-3'>";

                                echo "<table id='simulationchat' class='tbl_devis_calcul_tarif table-responsive'>";
                                    echo "<thead>";
                                        echo "<tr>";
                                            echo "<th></th>";
                                            echo "<th><strong><center>UN CHAT</center></strong></th>";
                                            echo "<th><strong><center>DEUX CHATS</center></strong></th>";
                                            echo "<th><strong><center>TROIS CHATS</center></strong></th>";
                                        echo "</tr>";
                                        echo "<tr>";
                                            echo "<th><strong><center>Nbr nuits</center></strong></th>";
                                            echo "<th>Tarif</th>";
                                            echo "<th>Tarif</th>";
                                            echo "<th>Tarif</th>";
                                        echo "</tr>";
                                    echo "</thead>";
                                    echo "<tbody>";

                                    echo "</tbody>";
                                echo "</table>";
                            echo "</div>";
                            
                        echo "</div>";
                echo "</div>";
                
                echo "<div class='card' style='width:100%;max-width: 100%;'>";
                    echo "<h2>Paramètres globaux : </h2><br />";
                    
                    echo "<label for='email_contact'><strong>Adresse mail d'envoi des devis : </strong>";
                    echo "<input type='text' name='email_contact' id='email_contact' required='required' value='".$row->email_contact."'>";
                    echo "<br /><hr /><br />";              
                    
                    
                    echo "<label for='txt_periode'><strong>Texte indicatif pour toute question :</strong>";
                    wp_editor($row->txt_question,"txt_question", array('textarea_name'=>'txt_question','textarea_rows'=>10, 'editor_class'=>'txt_question','media_buttons'=>false,'wpautop'=>false,'teeny'=>true));
                    echo "<br /><br />";

                    echo "<label for='txt_periode'><strong>Texte indicatif de la période de séjour :</strong> ";
                    wp_editor($row->txt_periode,"txt_periode", array('textarea_name'=>'txt_periode','textarea_rows'=>10, 'editor_class'=>'txt_periode','media_buttons'=>false,'wpautop'=>false));
                echo "</div>";
            
            
                
            
            echo "<p><center><button type='submit' style='font-size:18px;padding:10px 15px;margin-top:10px;' name='form_devis_parametre_submit' class='btn btn-success'>Enregistrer</button></center></p>";
            echo "</form>";
    }
    public static function devis_holidays()
    {
        global $wpdb;
        
        
        wp_enqueue_script( 'devis-parametre-js', plugins_url( '/js/devis_parametre.js', __FILE__ ));
        wp_enqueue_style( 'devis-parametre-css', plugins_url( '/css/devis_parametre.css', __FILE__ ));
        
        $year = (isset($_GET['y'])) ? $_GET['y'] : date('Y');
        $row = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}devis_jours_feries WHERE year=".$year." ORDER BY timestamp ASC");
        $rowHolidays = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}devis_vacances WHERE periode like '%-".$year."' ORDER BY DateDebut ASC");

        if(count($rowHolidays)==0):
            Devis::updateHolidays(($year-1)."-".$year);
            $rowHolidays = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}devis_vacances WHERE periode like '%-".$year."' ORDER BY DateDebut ASC");
        endif;

        echo "<div class='card' style='width:96%;max-width:96%;float:left'>";
        ?>
            <form method="GET" action="#" name="submityear">
                <input type="hidden" name="page" value="devis_holidays">
                <label for="y">Année :</label>
                <select name="y" id="y">
                    <?php for($i=date('Y');$i<date('Y')+10;$i++): ?>
                    <option value="<?php echo $i;?>" <?php if($year==$i) echo "selected='selected'";?>><?php echo $i;?></option>
                    <?php endfor;?>
                </select>
            </form>
        <?php 
        echo "</div>";
        echo "<div class='card' style='width:45%;max-width:45%;float:left'>";
            echo '<h2>Devis - Jours féries de l\'année '.$year.' <div class="wp-menu-image dashicons-before dashicons-update dashicons-update-right-blue-ferie" title="Restaurer les jours fériés pour l\'année '.$year.'"></div></h2><br />';
            echo "<table class='bordered' id='tbl-ferie' data-attr-id=".$year."  style='width:95%;border-collapse:collapse'>";
                echo "<thead>";
                    echo "<tr>";
                        echo "<th style='text-align:left'>Date</th>";
                        echo "<th style='text-align:left'>Nom</th>";
                    echo "</tr>";
                echo "</thead>";
                echo "<tbody>";
                    foreach($row as $r):
                        echo "<tr id='".$r->id."'>";
                            echo "<td>";
                                echo date("d/m/Y",$r->timestamp);
                            echo "</td>";
                            echo "<td>";
                                echo $r->nom;
                            echo "</td>";
                            echo "<td>
                                <button class='btn btn-danger btn-delete-ferie' title='Supprimer ce jour férié'><span class='dashicons dashicons-no'></span></button>
                            </td>";
                        echo "</tr>";
                    endforeach;
                echo "</tbody>";
                echo "<tfoot>";
                    echo "<tr><td colspan='3'><hr /></td></tr>";
                    echo "<tr>";
                        echo "<td><input type='date' name='newferiedate' value='".date('Y').'-'.date('m').'-'.date('d')."' id='newferiedate'></td>";
                        echo "<td><input type='text' name='newferienom' id='newferienom'></td>";
                        echo "<td><button class='btn btn-success btn-add-ferie' title='Ajouter ce jour férié'><span class='dashicons dashicons-yes'></span></button></td>";
                    echo "</tr>";
                echo "</tfoot>";
            echo "</table>";
        echo "</div>";
        echo "<div class='card' style='width:47%;max-width:47%;margin-left:1%;float:left'>";
            echo '<h2>Devis - Vacances scolaires de l\'année '.($year-1)."-".$year.'  - ZONE B - Académie d\'Orléans-Tours <div class="wp-menu-image dashicons-before dashicons-update dashicons-update-right-blue" title="Restaurer les périodes de vacances pour '.($year-1).'-'.$year.'"></div></h2><br />';
            echo "<table class='bordered' id='tbl-holiday' data-attr-id=".($year-1)."-".$year." style='width:95%;border-collapse:collapse'>";
                echo "<thead>";
                    echo "<tr>";
                        echo "<th style='text-align:left'>Période</th>";
                        echo "<th style='text-align:left'>Début</th>";
                        echo "<th style='text-align:left'>Fin</th>";
                        echo "<th style='text-align:left'></th>";
                    echo "</tr>";
                echo "</thead>";
                echo "<tbody>";
                    foreach($rowHolidays as $r):
                        echo "<tr id='".$r->id."'>";
                            echo "<td>";
                                echo $r->periode;
                            echo "</td>";
                            echo "<td>";
                                $ddeb = new \DateTime($r->DateDebut);
                                echo $ddeb->format("d/m/Y");
                            echo "</td>";
                            echo "</td>";
                            echo "<td>";
                                $dfin = ($r->DateFin=="0000-00-00 00:00:00") ? null : new \DateTime($r->DateFin);
                                echo (is_null($dfin)) ? "?" : $dfin->format("d/m/Y");
                            echo "</td>";
                            echo "<td><button class='btn btn-danger btn-delete-holiday'><span class='dashicons dashicons-no'></span></button></td>";
                        echo "</tr>";
                    endforeach;
                echo "</tbody>";
            echo "</table>";
        echo "</div>";
    }
    
    
    public static function updateHolidays($periode="2017-2018"){
        global $wpdb;
        $url = 'https://data.education.gouv.fr/api/records/1.0/search/?dataset=fr-en-calendrier-scolaire&q=zones%3DB+AND+annee_scolaire%3D'.$periode.'+AND+location%3DOrl%C3%A9ans-Tours&facet=description&facet=start_date&facet=end_date&facet=location&facet=zones&facet=annee_scolaire&timezone=Europe%2FParis';
        $data = Devis::getUrlContent($url);
        $d = json_decode($data);
        $wpdb->query("DELETE FROM {$wpdb->prefix}devis_vacances WHERE periode='".$periode."'");
        foreach($d->records as $j):
            $sql = "INSERT INTO  {$wpdb->prefix}devis_vacances(periode,zone,DateDebut,DateFin) VALUES ('".$periode."','ZONE B - Orléans-Tours','".(isset($j->fields->start_date) ? $j->fields->start_date : '')."','".(isset($j->fields->end_date) ? $j->fields->end_date : '')."')";
            $wpdb->query($sql);
        endforeach;
    }
    public static function updateJoursFeries($year){
        global $wpdb;
        $wpdb->query("DELETE FROM {$wpdb->prefix}devis_jours_feries WHERE year='".$year."'");
        $timestamp = Devis::getJoursFeries($year,true);
        foreach($timestamp as $k=>$t):
            $wpdb->query("INSERT INTO {$wpdb->prefix}devis_jours_feries (`year`,`timestamp`,`nom`,`etat`) VALUES (".$year.",".(array_keys($timestamp[$k])[0]).",'".$t[array_keys($timestamp[$k])[0]]."',1)");
        endforeach;
    }
    public static function getUrlContent($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $data = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return ($httpcode>=200 && $httpcode<300) ? $data : false;
    }
    public static function getJoursFeries($year = null, $withname=false)
    {
      if ($year === null)
      {
        $year = intval(date('Y'));
      }

      $easterDate  = easter_date($year);
      $easterDate+=60*5*60;
      $easterDay   = date('j', $easterDate);
      $easterMonth = date('n', $easterDate);
      $easterYear   = date('Y', $easterDate);
      if($withname):
        $holidays = array(
          // Dates fixes
          array(mktime(0, 0, 0, 1,  1,  $year)=>"1er janvier"),  // 1er janvier
          array(mktime(0, 0, 0, 5,  1,  $year)=>"Fête du travail"),  // Fête du travail
          array(mktime(0, 0, 0, 5,  8,  $year)=>"Victoire des alliés"),  // Victoire des alliés
          array(mktime(0, 0, 0, 7,  14, $year)=>"Fête nationale"),  // Fête nationale
          array(mktime(0, 0, 0, 8,  15, $year)=>"Assomption"),  // Assomption
          array(mktime(0, 0, 0, 11, 1,  $year)=>"Toussaint"),  // Toussaint
          array(mktime(0, 0, 0, 11, 11, $year)=>"Armistice"),  // Armistice
          array(mktime(0, 0, 0, 12, 25, $year)=>"Noel"),  // Noel

          // Dates variables
          array(mktime(0, 0, 0, $easterMonth, $easterDay + 1,  $easterYear)=>"Lundi de Paques"), // Lundi de Paques
          array(mktime(0, 0, 0, $easterMonth, $easterDay + 39, $easterYear)=>"Ascension"), // Ascension
          array(mktime(0, 0, 0, $easterMonth, $easterDay + 50, $easterYear)=>"Lundi de Pentecôte") 
        );

        sort($holidays);

        return $holidays;
      else:
        $holidays = array(
          // Dates fixes
          mktime(0, 0, 0, 1,  1,  $year),  // 1er janvier
          mktime(0, 0, 0, 5,  1,  $year),  // Fête du travail
          mktime(0, 0, 0, 5,  8,  $year),  // Victoire des alliés
          mktime(0, 0, 0, 7,  14, $year),  // Fête nationale
          mktime(0, 0, 0, 8,  15, $year),  // Assomption
          mktime(0, 0, 0, 11, 1,  $year),  // Toussaint
          mktime(0, 0, 0, 11, 11, $year),  // Armistice
          mktime(0, 0, 0, 12, 25, $year),  // Noel

          // Dates variables
          mktime(0, 0, 0, $easterMonth, $easterDay + 1,  $easterYear), // Lundi de Paques
          mktime(0, 0, 0, $easterMonth, $easterDay + 39, $easterYear), // Ascension
          mktime(0, 0, 0, $easterMonth, $easterDay + 50, $easterYear), // Lundi de Pentecôte
        );

        sort($holidays);

        return $holidays;
      endif;
    }
}
