<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'wpcontact');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', '');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+ofIJ}L3AtSP;P;x=~?5SDj/]j0etRqQ(H;WrrmwDUxTj2H3.c99yIm:{]uTS#U6');
define('SECURE_AUTH_KEY',  'hg]HLfN}H>-hCgoVcv=YyfGyf)M$%=AVu^XxI1c>Xd$+V-R]9aPm7GB+h.YCF3Jq');
define('LOGGED_IN_KEY',    'iqh09Z[PQ8>.BytUGWxTp`2& 7b?KKeS8bd4QF/ccz?~T:L/kmhq[A=+e5h42t5E');
define('NONCE_KEY',        '-kBX$DnNHBzKXn]7,c.Q~V2hL;77Upb[Iugf+fgZ)hdyy`vfSn7dl!PEL4w03+e&');
define('AUTH_SALT',        'W@l^=Pt|0GRBjW|r(t4*H+YOWp:eedzVt{t$?,tJewA}ROQDMM5X333 0#V+^He1');
define('SECURE_AUTH_SALT', 'l^Cji,2H:<+2J 8>z@+z/6iu2_->^fckl?|S/P8ZF&t%Y9J-*O,:.-Ynee{]KM5E');
define('LOGGED_IN_SALT',   'i.}#eiWY JT`-qg;`,G>.3h_O}9XPyx5Nam+ TdCxdL,Vdj3KK&z,^qVrE8+%_:Y');
define('NONCE_SALT',       'g(8HnSYX$P!@$PkDM}x@7Z|ESN1){,%b8 9OXXi,xa:.QG^Cy/b$2a3s)BDj:c).');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
